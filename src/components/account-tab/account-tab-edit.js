import React, { Component } from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import { FieldWrapper } from '../../components';

const data = {
  firstName: 'Mohamed',
  lastName: 'AboSoliman',
  email: 'Abosolimanmas123@gmail.com',
  mobile: '01116586783',
  address: '',
  zipcode: '',
  city: '',
  state: ''
};

export default class AccountTabEdit extends Component {
  submit = values => {};
  render() {
    return (
      <div className="page__content container">
        <h3 className="account__title">Edit Account</h3>
        <Formik // Initialized values using name
          initialValues={{
            first_name: null,
            last_name: null,
            email: null,
            phone: null,
            address: null,
            zipcode: null,
            city: null,
            state: null
          }}
          validationSchema={Yup.object().shape({
            first_name: Yup.string()
              .trim()
              .nullable()
              .required('First name is required'),
            last_name: Yup.string()
              .trim()
              .nullable(),
            email: Yup.string()
              .trim()
              .nullable()
              .email('Please enter a valid email'),
            phone: Yup.mixed()
              .test('phone', 'Phone number is required', val => {
                return val !== undefined && val !== null;
              })
              .test('phone', 'Phone number must have numeric values', val => {
                return isFinite(val) && !isNaN(val) && val > 0;
              }),
            address: Yup.string()
              .trim()
              .nullable(),
            zipcode: Yup.number('This field must have numeric values'),
            city: Yup.string()
              .trim()
              .nullable(),
            state: Yup.string()
              .trim()
              .nullable()
          })}
          onSubmit={(
            values,
            { setSubmitting, setErrors /* setValues and other goodies */ }
          ) => {
            // You can access values here eg. values.first_name
            this.submit(values);
            setSubmitting(false);
          }}
          render={({
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            setFieldTouched,
            isSubmitting
          }) => {
            return (
              <Form className="edit-account__form" onSubmit={handleSubmit}>
                <div className="grid grid--2">
                  <FieldWrapper
                    type="text"
                    name="first_name"
                    id="first-name"
                    placeholder="First Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="text"
                    name="last_name"
                    id="last-name"
                    placeholder="Last Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="text"
                    name="phone"
                    id="phone"
                    placeholder="Phone"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="text"
                    name="address"
                    id="address"
                    placeholder="Address"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="number"
                    name="zipcode"
                    id="zipcode"
                    placeholder="Zipcode"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="text"
                    name="city"
                    id="city"
                    placeholder="City"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="text"
                    name="state"
                    id="state"
                    placeholder="State"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                </div>
                <div className="form__actions">
                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="form__action btn"
                  >
                    Save
                  </button>
                </div>
              </Form>
            );
          }}
        />
      </div>
    );
  }
}
