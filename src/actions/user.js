import { APIInstance } from '../utils/api';

export const USER_LOGIN = {
  ROOT: 'user_login',
  PENDING: 'user_login_PENDING',
  FULFILLED: 'user_login_FULFILLED',
  REJECTED: 'user_login_REJECTED'
};

export const USER_LOGOUT = {
  ROOT: 'user_logout',
  PENDING: 'user_logout_PENDING',
  FULFILLED: 'user_logout_FULFILLED',
  REJECTED: 'user_logout_REJECTED'
};

export const USER_REGISTER = {
  ROOT: 'user_register',
  PENDING: 'user_register_PENDING',
  FULFILLED: 'user_register_FULFILLED',
  REJECTED: 'user_register_REJECTED'
};

export function userLogin({ email, password }, callback) {
  return {
    type: USER_LOGIN.ROOT,
    payload: APIInstance.login(email, password)
      .then(res => {
        callback && callback(null);
        return res;
      })
      .catch(err => {
        console.error(err);
        callback && callback(err);
        throw err;
      })
  };
}

export function userLogout(callback) {
  return {
    type: USER_LOGOUT.ROOT,
    payload: APIInstance.logout()
      .then(res => {
        callback && callback();
        return res;
      })
      .catch(err => {
        callback && callback(err);
        throw err;
      })
  };
}

export function userRegister(values) {
  return {
    type: USER_REGISTER.ROOT,
    payload: APIInstance.register(values)
  };
}

export function checkUserAuth() {
  return {
    type: USER_LOGIN.ROOT,
    payload: new Promise((resolve, reject) => {
      const authHeadersStr = localStorage.getItem('authHeaders');
      const authUserStr = localStorage.getItem('authUser');
      if (authHeadersStr) {
        const authHeaders = JSON.parse(authHeadersStr);
        const authUser = JSON.parse(authUserStr);
        APIInstance.updateTokens(authHeaders);
        resolve({ data: authUser });
      } else {
        reject({ message: 'Not authorized' });
      }
    })
  };
}
