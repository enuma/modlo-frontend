import React, { Component } from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import { FieldWrapper } from '../../components';
import { Banner } from '../../components';

export default class Contact extends Component {
  submit = values => {};
  render() {
    return (
      <main className="contact">
        <section className="contact__info">
          <Banner
            height={420}
            imageURL={''}
            overlay={{ r: 15, g: 10, b: 81, alpha: 1 }}
          >
            <div className="container contact__content">
              <div className="contact__name">Mahmoud Asseel</div>
              <ul className="contact__details">
                <li className="contact__detail contact__address">
                  <i className="icon icon-location" />
                  <p className="contact__address__text contact__detail__text">
                    1900 Pico Blvd, Santa Monica, CA 90405
                  </p>
                </li>
                <li className="contact__detail contact__email">
                  <i className="icon icon-email" />
                  <p className="contact__email__text contact__detail__text">
                    <a href="mailto:mahmoud.asseel@modlo.co">
                      mahmoud.asseel@modlo.co
                    </a>
                  </p>
                </li>
                <li className="contact__detail contact__phone">
                  <i className="icon icon-phone" />
                  <p className="contact__phone__text contact__detail__text">
                    <a href="tel:+(2) 01001001557">+(2) 01001001557</a>
                  </p>
                </li>
              </ul>
            </div>
          </Banner>
        </section>
        <section className="contact__form-wrapper">
          <div className="container">
            <Formik
              initialValues={{
                first_name: null,
                last_name: null,
                email: null,
                message: null
              }}
              validationSchema={Yup.object().shape({
                first_name: Yup.string()
                  .trim()
                  .nullable()
                  .required('First name is required'),
                last_name: Yup.string()
                  .trim()
                  .nullable()
                  .required('Last name is required'),
                email: Yup.string()
                  .trim()
                  .nullable()
                  .email('Please enter a valid email'),
                message: Yup.string()
                  .trim()
                  .nullable()
                  .email('Please enter a valid email')
              })}
              onSubmit={(values, { setSubmitting, setErrors, setValues }) => {
                this.submit(values);
                setSubmitting(false);
              }}
              render={({
                values,
                touched,
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue,
                setFieldTouched,
                isSubmitting
              }) => {
                return <Form className="contact__form" onSubmit={handleSubmit}>
                    <div className="grid contact__form__section">
                      <FieldWrapper type="text" name="first_name" id="first-name" placeholder="First Name" onChange={handleChange} onBlur={handleBlur} touched={touched} errors={errors} />
                      <FieldWrapper type="text" name="last_name" id="last-name" placeholder="Last Name" onChange={handleChange} onBlur={handleBlur} touched={touched} errors={errors} />
                      <FieldWrapper type="email" name="email" id="email" placeholder="Email" onChange={handleChange} onBlur={handleBlur} touched={touched} errors={errors} />
                      <FieldWrapper type="text" name="phone" id="phone" placeholder="Phone" onChange={handleChange} onBlur={handleBlur} touched={touched} errors={errors} />
                    </div>
                    <FieldWrapper type={null} component="textarea" name="message" id="message" placeholder="Message" onChange={handleChange} onBlur={handleBlur} touched={touched} errors={errors} />
                    <div className="form__actions">
                      <button className="form__action btn" disabled={isSubmitting}>
                        Send
                      </button>
                    </div>
                  </Form>;
              }}
            />
          </div>
        </section>
      </main>
    );
  }
}
