import React, { Component } from "react";
import { connect } from "react-redux";
import { Table } from "../";

import { fetchMyProperties } from "../../actions";

const items = [
  {
    id: 1,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 2,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 3,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 4,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 5,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 6,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 7,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 8,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  },
  {
    id: 9,
    image: "",
    name: "Sandow lakes ranch",
    address: "Egypt",
    price: "250,000 $"
  }
];

class MyProperties extends Component {

  componentDidMount() {
    this.props.fetchMyProperties();
  }

  render() {
    const { myProperties, isLoading, isEmpty, isLoaded } = this.props;
    const actions = {'show': true};
    {isLoading && <p>Loading</p>}
    {isEmpty && <p>Empty</p>}
    return (
      <Table
        heads= {['Image', 'Name', 'location', 'Price', 'Actions']}
        data= {isLoaded ? myProperties: []}
        actions = {actions}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    myProperties: state.my_properties.list,
    ...state.my_properties_flags
  };
}
export default connect(
  mapStateToProps,
  {
    fetchMyProperties
  }
)(MyProperties);

