var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var sassGlob = require("gulp-sass-glob");

var sassFilesDirs = ["src/config", "src/components", "src/pages"];

// ... variables
var inputFile = "src/main.scss";
var watchedFiles = sassFilesDirs.map(sassDir => sassDir + "/**/*.scss");
watchedFiles.push("src/main.scss");
var outputDir = "src/";

var sassOptions = {
  errLogToConsole: true,
  outputStyle: "expanded"
};

var autoprefixerOptions = {
  browsers: ["last 2 versions", "> 5%", "Firefox ESR"]
};

gulp.task("sass", function() {
  return (
    gulp
      .src(inputFile)
      // .pipe(sourcemaps.init())
      .pipe(sassGlob())
      .pipe(sass(sassOptions).on("error", sass.logError))
      // .pipe(sourcemaps.write())
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(gulp.dest(outputDir))
  );
});
gulp.task("watch", function() {
  return (
    gulp
      // Watch the input folder for change,
      // and run `sass` task when something happens
      .watch(watchedFiles, ["sass"])
      // When there is a change,
      // log a message in the console
      .on("change", function(event) {
        console.log(
          "File " + event.path + " was " + event.type + ", running tasks..."
        );
      })
  );
});

gulp.task("default", ["sass", "watch" /*, possible other tasks... */]);
gulp.task("build", ["sass"]);
