import React from 'react';
import { NavLink } from 'react-router-dom';

class AccountTabInformation extends React.Component {
  render() {
    const {user} = this.props;
    return (
      <div className="account-information">
        <div>
          <h2 className="account-information__title">Contact Information</h2>
          <div className="account-information__contact">
            <i className="icon icon-contact" />
            <div>
              <p>{user.email}</p>
              <p>{user.phone}</p>
            </div>
          </div>
        </div>
        <div>
          <NavLink
            exact
            to={'/profile/edit-account'}
            className="account-action"
          >
            <i className="icon icon-account" />
            <span>Edit Account</span>
          </NavLink>
        </div>
        <div>
          <NavLink
            exact
            to={'/profile/change-password'}
            className="account-action"
          >
            <i className="icon icon-password" />
            <span>Change Password</span>
          </NavLink>
        </div>
      </div>
    );
  }
}

export default AccountTabInformation;
