import React, { Component } from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import { FieldWrapper } from '../../components';

class AccountTabPassword extends Component {
  submit = values => {};
  render() {
    return (
      <div className="page__content container">
        <h3 className="account__title">Change Password</h3>
        <Formik
          initialValues={
            {
              first_name: null,
              last_name: null,
              email: null,
              phone: null,
              address: null,
              zipcode: null,
              city: null,
              state: null
            } // Initialized values using name
          }
          validationSchema={Yup.object().shape({
            old_password: Yup.string()
              .nullable()
              .required('Old Password is required'),
            new_password: Yup.string()
              .nullable()
              .required('New Password is Required'),
            confirm_new_password: Yup.string()
              .oneOf(
                [Yup.ref('newPassword'), null],
                'New Passsword and Confirm New Password must match'
              )
              .required('New Password Confirm is required')
          })}
          onSubmit={(
            values,
            { setSubmitting, setErrors /* setValues and other goodies */ }
          ) => {
            // You can access values here eg. values.first_name
            this.submit(values);
            setSubmitting(false);
          }}
          render={({
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            setFieldTouched,
            isSubmitting
          }) => {
            return (
              <Form onSubmit={handleSubmit}>
                <div className="grid grid--2">
                  <FieldWrapper
                    type="password"
                    name="old_password"
                    id="old_password"
                    placeholder="Old Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="password"
                    name="new_password"
                    id="new_password"
                    placeholder="New Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                  <FieldWrapper
                    type="password"
                    name="confirm_new_password"
                    id="confirm_new_password"
                    placeholder="Confirm New Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    touched={touched}
                    errors={errors}
                  />
                </div>
                <div className="form__actions">
                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="form__action btn"
                  >
                    Save
                  </button>
                </div>
              </Form>
            );
          }}
        />
      </div>
    );
  }
}

export default AccountTabPassword;
