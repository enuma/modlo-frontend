import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import { FieldWrapper } from '../../components';
import { Banner } from '../../components';
import { userLogin } from '../../actions';
import { APIInstance } from '../../utils/api';

class Login extends Component {
  submit = (values, setSubmitting, setErrors) => {
    let {history} = this.props;
    return this.props.userLogin(values, (err) => {
      setSubmitting(false);
      if (err) {
        setErrors({ _error: err.response.data.errors[0] });
      } else {
        history.push('/profile');
      }
    });
  };
  render() {
    return (
      <main className="login overlapping-title">
        <section className="login__form-wrapper">
          <div className="page__banner" />
          <div className="page__content container">
            <h2 className="page__title">Log in</h2>
            <Formik
              initialValues={{ email: '', password: '' }}
              validationSchema={Yup.object().shape({
                email: Yup.string()
                  .trim()
                  .nullable()
                  .email('Please enter a valid email'),
                password: Yup.string()
                  .nullable()
                  .required('Password is required')
              })}
              onSubmit={(
                values,
                { setSubmitting, setErrors /* setValues and other goodies */ }
              ) => {
                // You can access values here eg. values.first_name
                this.submit(values, setSubmitting, setErrors);
              }}
              render={({
                values,
                touched,
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                setFieldValue,
                setFieldTouched,
                isSubmitting
              }) => {
                return (
                  <Form className="login__form" onSubmit={handleSubmit}>
                    {errors && errors._error && <p className="error">{errors._error}</p>}
                    <FieldWrapper
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      touched={touched}
                      errors={errors}
                    />
                    <FieldWrapper
                      type="password"
                      name="password"
                      id="password"
                      placeholder="Password"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      touched={touched}
                      errors={errors}
                    />
                    <div className="form__actions">
                      <button
                        className="form__action btn"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        Login
                      </button>
                      <p className="form__action form__action--forgot">
                        <Link to="/register">Forgot password ?</Link>
                      </p>
                      <p className="form__action form__action--register">
                        New user?{' '}
                        <Link to="/register" className="form__action__link">
                          Register
                        </Link>
                      </p>
                    </div>
                  </Form>
                );
              }}
            />
          </div>
        </section>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.user_flags
  };
}

export default connect(
  mapStateToProps,
  {
    userLogin
  }
)(Login);
