export { default as Home } from "./home/home";
export { default as PropertiesSearch } from "./properties/search";
export { default as Properties } from "./properties/properties";
export { default as Profile } from "./profile/profile";
export {default as Yachts } from './yachts/yachts';
export {default as YachtsSearch} from './yachts/search';
export {default as Contact} from './contact/contact';
export {default as Register} from './register/register';
export {default as Login} from './login/login';
