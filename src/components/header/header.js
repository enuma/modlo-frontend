import React from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import { userLogout } from '../../actions';
// import logo from '../../images/logo.png';

const navLinks = [
  { url: '/', title: 'Home' },
  { url: '/properties', title: 'Properties' },
  { url: '/yachts', title: 'Yachts' },
  { url: '/charters', title: 'Charters' },
  { url: '/catering', title: 'Catering' }
];

export default class Header extends React.Component {
  render() {
    const { activeRoute, user, logout } = this.props;
    const isAuthorized = !!(user && Object.keys(user).length);
    return (
      <header className="header container">
        <h1 className="header__logo">
          <NavLink exact to="/">
            <img src="/images/logo.png" alt="Modlo Logo" />
          </NavLink>
        </h1>
        <nav className="header__nav-wrapper">
          <ul className="header__nav">
            {navLinks.map((navLink, index) => (
              <li
                key={`${index}-${navLink}`}
                className={classNames(
                  { active: activeRoute === navLink.url },
                  'header__nav-link'
                )}
              >
                <NavLink exact to={navLink.url}>
                  {navLink.title}
                </NavLink>
              </li>
            ))}
          </ul>
        </nav>
        <div className="header__actions">
          {!isAuthorized && (
            <NavLink
              exact
              to="/login"
              className="header__action btn btn--round"
            >
              Sign In
            </NavLink>
          )}
          {isAuthorized && (
            <button
              className="header__action btn btn--round"
              onClick={logout}
            >
              Sign Out
            </button>
          )}
        </div>
      </header>
    );
  }
}
