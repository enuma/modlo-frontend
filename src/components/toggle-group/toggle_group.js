import React, { Component } from "react";
import classNames from 'classnames';

const ToggleGroup = ({ onChange, checkedID, elementsData }) => {
  return (
    <div className="toggle-group">
      {elementsData.map((element, index) => {
        return (
          <div
            className={classNames("toggle-group__toggle", {
              active: checkedID === element.input.id
            })}
          >
            <label
              htmlFor={element.input.id}
              className={"toggle-group__toggle__label"}
            >
              {element.label}
              <input
                type="radio"
                id={element.input.id}
                name={element.input.name}
                value={element.input.value}
                checked={checkedID === element.input.id}
                className="toggle-group__toggle__input"
                onChange={onChange}
              />
            </label>
          </div>
        );
      })}
    </div>
  );
};

export default ToggleGroup;
