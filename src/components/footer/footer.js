import React from "react";
import { Link } from "react-router-dom";

const aboutNavLinks = [
  { url: "/properties", title: "Properties" },
  { url: "/yachts", title: "Yachts" },
  { url: "/charters", title: "Charters" },
  { url: "/catering", title: "Catering" }
];

const servicesNavLinks = [
  { url: "/diving", title: "Diving" },
  { url: "/snorkeling", title: "Snorkeling" },
  { url: "/housekeeping", title: "HouseKeeping" },
  { url: "/airport", title: "Airport Pickup" }
];
export default class Footer extends React.Component {
  render() {
    const { activeRoute } = this.props;
    return <footer>
        <div className="container">
          <div className="footer__modlo">
            <div className="footer__modlo__logo-wrapper">
              <img src="/images/logo2.png" alt="Modlo Logo" />
            </div>
            <p className="footer__modlo__description">
              The Modlo experience is a concierge service that is designed
              to ease your navigation and enhance your experience in Gouna.
            </p>
          </div>
          <div className="footer__nav">
            <div className="footer__nav__section">
              <h2 className="footer__nav__title">About Us</h2>
              <ul className="footer__nav__links footer__nav__content">
                {aboutNavLinks.map((navLink, index) => {
                  return <li key={index} className="footer__nav__link">
                      <Link to={navLink.url}>{navLink.title}</Link>
                    </li>;
                })}
              </ul>
            </div>
            <div className="footer__nav__section">
              <h2 className="footer__nav__title">Other Services</h2>
              <ul className="footer__nav__links footer__nav__content">
                {servicesNavLinks.map((navLink, index) => {
                  return <li key={index} className="footer__nav__link">
                      <Link to={navLink.url}>{navLink.title}</Link>
                    </li>;
                })}
              </ul>
            </div>
            <div className="footer__nav__section">
              <h2 className="footer__nav__title">Support</h2>
              <p className="footer__nav__description footer__nav__content">
                Feel free to get in touch with us. We will try to answer all
                your questions as soon as possible.
              </p>
              <div className="footer__nav__actions">
                <Link to="/contact" className="btn">
                  Contact Us
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__copyrights">
          <div className="container">
            <p className="footer__copyrights__text">
              Created by <strong>Phantasm</strong>. All rights reserved.
            </p>
            <div className="footer__copyrights__icons">
              <i className="icon icon-facebook" />
              <i className="icon icon-youtube" />
              <i className="icon icon-twitter" />
            </div>
          </div>
        </div>
      </footer>;
  }
}
