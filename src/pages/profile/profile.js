import React, { Component } from 'react';
import { UserBanner } from '../../components';
import ProfileTabs from './profile-tabs';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { checkUserAuth } from '../../actions';

class Account extends Component {
  componentDidMount() {
    let { user, checkUserAuth } = this.props;
    if (!user || !Object.keys(user).length) {
      checkUserAuth();
    }
  }

  render() {
    let { user } = this.props;
    if (!user || !Object.keys(user).length) {
      return <p>401</p>;
    } else {
      return (
        <main>
          <UserBanner
            avatar={user.image}
            name={(user.first_name || '') + (user.last_name || '')}
            role={user.role}
          />
          <ProfileTabs  user={user}/>
        </main>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    ...state.user_flags
  };
}

export default connect(mapStateToProps, {
  checkUserAuth
})(Account);
