import React from "react";
import Slider from "react-slick";

import { Banner, Grid } from "../../components";
const images = [1, 2, 3, 4, 5, 6].map(key => `/images/home/banner/${key}.jpg`);
const services = [
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  },
  {
    title: "Interior Design",
    subTitle: "Enthusiastically parallel task cross functional convergence"
  }
].map((service, index) =>
  Object.assign(service, {
    image: `url('/images/home/services/${index + 1}.jpg')`
  })
);

export default class Home extends React.Component {
  renderServiceCard(index, service) {
    return (
      <div
        key={index}
        className="services__card card"
        style={{
          backgroundImage: service.image,
          backgroundSize: "cover",
          backgroundPosition: "center"
        }}
      >
        <div className="card__content">
          <h3 className="card__title">{service.title}</h3>
          <p className="card__subtitle">{service.subTitle}</p>
        </div>
      </div>
    );
  }
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <main>
        <section className="home__banner banner">
          <Slider className="banner__slider slider" {...settings}>
            {images.map((image, index) => {
              return (
                <div key={index} className="slider__slide">
                  <Banner
                    className="slider__slide-content"
                    imageURL={image}
                    height={743}
                  />
                  {/* <div
                    className="slider__slide-content"
                    style={{ backgroundImage: image, backgroundSize: "cover", backgroundPosition: 'center' }}
                  /> */}
                </div>
              );
            })}
          </Slider>
        </section>
        <section className="services container">
          <h2 className="services__title"> Our Services </h2>
          <div className="grid">
            {services.map((service, index) =>
              this.renderServiceCard(index, service)
            )}
          </div>
        </section>
      </main>
    );
  }
}
