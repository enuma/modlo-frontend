import React from "react";
import "rc-slider/assets/index.css";
import Tooltip from "rc-tooltip";
import {Range, Handle} from "rc-slider";
import { connect } from "react-redux";

import { fetchYachts } from "../../actions";
import { ToggleGroup, Grid, PropertyCard } from "../../components";

// const createSliderWithTooltip = Slider.createSliderWithTooltip;
// const Range = createSliderWithTooltip(Slider.Range);
const handle = props => {
  const { value, dragging, index, offset, ...restProps } = props;
  console.log(restProps);
  return (
    // <Tooltip
    //   prefixCls="rc-slider-tooltip"
    //   overlay={value}
    //   visible
    //   defaultVisible
    //   arrowContent={null}
    //   placement="top"
    //   key={index}
    // >
    <div className="custom-search-handle" style={{left: offset + '%'}}>
      <span className="custom-search-handle__text">{value}</span>
      <Handle value={value} {...restProps} />
    </div>
    // </Tooltip>
  );
};
const toggleGroupData = [
  {
    label: "Exterior",
    input: {
      id: "exterior",
      name: "view-type",
      value: "exterior"
    }
  },

  {
    label: "Interior",
    input: {
      id: "interior",
      name: "view-type",
      value: "interior"
    }
  },
  {
    label: "View",
    input: {
      id: "view",
      name: "view-type",
      value: "view"
    }
  }
];

class YachtsSearch extends React.Component {
  componentDidMount() {
    const {yachts, fetchYachts} = this.props;
    if (!(yachts && yachts.length)) {
      fetchYachts();
    }
  }

  toggle(ev) {
    this.setState({
      checked: ev.target.id,
      viewType: ev.target.value
    });
  }
  render() {
    let checkedID = (this.state || {}).checked || 'exterior';
    const {yachts, isLoading, isLoaded} = this.props;
    return (
      <main className="yachts-search">
        {/* <section className="filters">
          <div className="container filters__wrapper">
            <div className="filters__result">
              <span className="filters__result__count">1600</span>{" "}
              <span className="filters__result__text">Results found</span>
            </div>
            <div className="filters__price-range">
              <span className="filters__price-range__title">Price Range</span>
              <Range
                className="filters__price-range__slider"
                min={0}
                max={20}
                defaultValue={[3, 10]}
                handle={handle}
              />
            </div>
            <div className="filters__toggle">
              <ToggleGroup
                elementsData={toggleGroupData}
                checkedID={checkedID}
                onChange={ev => this.toggle(ev)}
              />
            </div>
          </div>
        </section> */}
        <section className="yachts__grid">
          <div className="container">
            <div className="grid">
              {isLoaded && yachts.map(yacht => {
                return (
                  <PropertyCard
                    key={yacht.id}
                    property={yacht}
                  />
                );
              })}
              {isLoading && <p>Loading</p>}
            </div>
          </div>
        </section>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    yachts: state.yachts.list,
    ...state.yachts_flags
  };
}

export default connect(
  mapStateToProps,
  {
    fetchYachts
  }
)(YachtsSearch);
