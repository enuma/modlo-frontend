import React, { Component } from "react";
import classNames from "classnames";


import { Banner, ToggleGroup } from "../../components";

const toggleGroupData = [
  {
    label: "For Sale",
    input: {
      id: "for-sale",
      name: "search-type",
      value: "for-sale"
    }
  },

  {
    label: "Rentals",
    input: {
      id: "rentals",
      name: "search-type",
      value: "rentals"
    }
  }
];
class Yachts extends Component {

  submit(ev) {
    this.props.history.push("/yachts/search/");
    console.log("Submit form");
    this.setState({ checked: null });
  }
  toggle(ev) {
    console.log("Toggle");
    this.setState(
      {
        checked: ev.target.id,
        searchType: ev.target.value
      },
      () => console.log(this.state)
    );
  }
  handleInputChange(ev) {
    let val = ev.target.value;
    this.setState({ q: val });
  }
  render() {
    let checkedID = (this.state || {}).checked || "for-sale";
    return (
      <main className="yachts">
        <Banner
          imageURL={"/images/yachts/banner.jpg"}
          height={770}
          overlay={{ alpha: 0.3 }}
          className="yachts__banner"
        >
          <form onSubmit={ev => this.submit(ev)}>
            <ToggleGroup
              elementsData={toggleGroupData}
              checkedID={checkedID}
              onChange={ev => this.toggle(ev)}
            />
            <div className="yachts__search">
              <input
                type="text"
                className="yachts__search__input"
                name="q"
                placeholder="Country, Region, City, Postal Code or Yacht ID"
                onChange={ev => this.handleInputChange(ev)}
              />
              <button type="submit" className="yachts__search__button btn">
                <i className="icon icon-search" />
                Search
              </button>
            </div>
          </form>
        </Banner>
      </main>
    );
  }
}

