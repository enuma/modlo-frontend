import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import {
  Home,
  PropertiesSearch,
  Properties,
  Profile,
  Yachts,
  YachtsSearch,
  Contact,
  Register,
  Login
} from '../../pages';
import { userLogout } from '../../actions';
import Header from '../header/header';
import Footer from '../footer/footer';

class App extends Component {
  logout = () => {
    this.props.userLogout();
  };

  render() {
    const { user } = this.props;
    return (
      <Router>
        <div className="App">
          <Header user={user} logout={this.logout} />
          <Switch>
            <Route component={Profile} path="/profile" />
            <Route component={Login} path="/login" />
            <Route component={Register} path="/register" />
            <Route component={Contact} path="/contact" />
            {/* <Route component={YachtsSearch} path="/yachts/search" /> */}
            <Route component={YachtsSearch} path="/yachts" />
            <Route component={PropertiesSearch} path="/properties/search" />
            <Route component={Properties} path="/properties" />
            <Route component={Home} path="/" />
          </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(
  mapStateToProps,
  {
    userLogout
  }
)(App);
