import { FETCH_YACHTS } from "../actions";

export default function yachtsReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case FETCH_YACHTS.FULFILLED:
      newState = { list: action.payload.data.map(property => {
        return {
          id: property.id,
          price: property.price,
          location: property.address,
          name: property.name || 'no name for now',
          image: property.image || "" || "/images/properties/property.jpg",
        };
      })};
      break;
    default:
      newState = state;
  }

  return newState;
}

export function yachtsFlagsReducer(state={}, action) {
  let newState;
  switch (action.type) {
    case FETCH_YACHTS.PENDING:
      newState = { isLoading: true, isLoaded: false, isError: false };
      break;
    case FETCH_YACHTS.FULFILLED:
      newState = { isLoading: false, isLoaded: true, isError: false };
      break;
    case FETCH_YACHTS.REJECTED:
      newState = {
        isLoading: false,
        isLoaded: false,
        isError: true,
        error: action.payload
      };
      break;
    default:
      newState = state;
  }

  return newState;
}