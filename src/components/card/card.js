import React from "react";
import classNames from "classnames";

const Card = ({
  className,
  image,
  title,
  subtitle,
  price,
  priceUnit,
  extraInfo
}) => {
  return (
    <div className={classNames("card", className)}>
      <div className="card__image-wrapper">
        <img className="card__image" src={image} alt="Card Image" />
      </div>
      <div className="card__content">
        <div className="card__content__header">
          <h3 className="card__title">{title}</h3>
          <p className="card__subtitle">{subtitle}</p>
          <span className="card__star">star</span>
        </div>
        <div className="card__misc">
          <div className="card__extra">
            {extraInfo &&
              Object.keys(extraInfo).filter(key => !!extraInfo[key]).map((key, index) => {
                return (
                  <div key={index} className="card__extra__wrapper">
                    <span className="card__extra__value">{extraInfo[key]}</span>
                    <span className="card__extra__label">{key}</span>
                  </div>
                );
              })}
          </div>
          <div className="card__price-wrapper">
            <span className="card__price">{price}</span>
            <span className="card__price-unit">{priceUnit}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
