import { FETCH_PROPERTIES, FETCH_MY_PROPERTIES } from "../actions";

export default function propertiesReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case FETCH_PROPERTIES.FULFILLED:
      newState = { list: action.payload.data.map(property => {
        return {
          id: property.id,
          price: property.price,
          location: property.address,
          name: property.name || 'no name for now',
          image: property.image || "" || "/images/properties/property.jpg",
          beds: 8,
          baths: 3,
        };
      })};
      break;
    default:
      newState = state;
  }

  return newState;
}

export function propertiesFlagsReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case FETCH_PROPERTIES.PENDING:
      newState = { isLoading: true, isLoaded: false, isError: false };
      break;
    case FETCH_PROPERTIES.FULFILLED:
      newState = { isLoading: false, isLoaded: true, isError: false };
      break;
    case FETCH_PROPERTIES.REJECTED:
      newState = {
        isLoading: false,
        isLoaded: false,
        isError: true,
        error: action.payload
      };
      break;
    default:
      newState = state;
  }

  return newState;
}

export function MyPropertiesReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case FETCH_MY_PROPERTIES.FULFILLED:
      newState = { list: action.payload.data.map(property => {
        return {
          id: property.id,
          price: property.price || '-',
          location: property.address,
          name: property.name || 'no name for now',
          image: property.image || "/images/generic/dummy.png",
        };
      })};
      break;
    default:
      newState = state;
  }

  return newState;
}

export function MyPropertiesFlagsReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case FETCH_MY_PROPERTIES.PENDING:
      newState = { isLoading: true, isLoaded: false, isError: false };
      break;
    case FETCH_MY_PROPERTIES.FULFILLED:
      newState = { isLoading: false, isLoaded: true, isError: false };
      break;
    case FETCH_MY_PROPERTIES.REJECTED:
      newState = {
        isLoading: false,
        isLoaded: false,
        isError: true,
        error: action.payload
      };
      break;
    default:
      newState = state;
  }

  return newState;
}
