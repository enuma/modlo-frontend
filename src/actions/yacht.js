import {APIInstance} from '../utils/api';

export const FETCH_YACHTS = {
  ROOT: 'fetch_yachts',
  PENDING: 'fetch_yachts_PENDING',
  FULFILLED: 'fetch_yachts_FULFILLED',
  REJECTED: 'fetch_yachts_REJECTED',
}

export function fetchYachts(contract_type, query) {
  return {
    type: FETCH_YACHTS.ROOT,
    payload: APIInstance.searchYachts(contract_type, query),
    meta: {
      contract_type,
      query
    }
  }
}

export const FETCH_YACHT = {
  ROOT: 'fetch_yacht',
  PENDING: 'fetch_yacht_PENDING',
  FULFILLED: 'fetch_yacht_FULFILLED',
  REJECTED: 'fetch_yacht_REJECTED',
}

export function fetchYacht(yachtID) {
  return {
    type: FETCH_YACHT.ROOT,
    payload: APIInstance.getYacht(yachtID),
    meta: {
      yachtID
    }
  }
}