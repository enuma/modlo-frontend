import React from "react";
import { Card } from "../";
const PropertyCard = ({ property }) => {
  let cardData = {
    image: property.image || "/images/properties/property.jpg",
    title: property.name,
    subtitle: property.location,
    price: property.price,
    priceUnit: "American Dollar",
    extraInfo: {
      Beds: property.beds,
      Baths: property.baths
    }
  };
  return <Card className="card property-card" {...cardData} />;
};
export default PropertyCard;
