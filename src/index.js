import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reduxPromiseMiddleware from "redux-promise-middleware";

// import "./node_modules/slick-carousel/slick/slick.css";
// import "./node_modules/slick-carousel/slick/slick-theme.css";

import rootReducer, { initialState } from "./reducers";
import { App } from "./components";
import registerServiceWorker from "./registerServiceWorker";

import "./main.css";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const CHANNEL_NAME = "sw-messages";

const createStoreWithMiddleware = () => {
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(reduxPromiseMiddleware()))
  );
};
ReactDOM.render(
  <Provider store={createStoreWithMiddleware()}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
