import { USER_LOGIN, USER_LOGOUT, USER_REGISTER } from '../actions';

export default function userReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case USER_LOGIN.FULFILLED:
      newState = action.payload.data.data;
      break;
    case USER_LOGOUT.FULFILLED:
      newState = {};
      break;
    default:
      newState = state;
  }

  return newState;
}

export function userFlagsReducer(state = {}, action) {
  let newState;
  switch (action.type) {
    case USER_LOGIN.PENDING:
    case USER_LOGOUT.PENDING:
    case USER_REGISTER.PENDING:
      newState = { isLoading: true, isLoaded: false, isError: false };
      break;
    case USER_LOGIN.FULFILLED:
    case USER_LOGOUT.FULFILLED:
    case USER_REGISTER.FULFILLED:
      newState = { isLoading: false, isLoaded: true, isError: false };
      break;
    case USER_LOGIN.REJECTED:
    case USER_LOGOUT.REJECTED:
    case USER_REGISTER.REJECTED:
      newState = { isLoading: false, isLoaded: false, isError: true, error: action.payload };
      break;
    default:
      newState = state;
  }

  return newState;
}
