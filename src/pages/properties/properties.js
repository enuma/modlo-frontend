import React, { Component } from "react";
import classNames from "classnames";

import { Banner, ToggleGroup } from "../../components";
const toggleGroupData = [
  {
    label: "For Sale",
    input: {
      id: "for-sale",
      name: "search-type",
      value: "for-sale"
    }
  },

  {
    label: "Rentals",
    input: {
      id: "rentals",
      name: "search-type",
      value: "rentals"
    }
  },
  {
    label: "Developments",
    input: {
      id: "developments",
      name: "search-type",
      value: "developments"
    }
  }
];
export default class Properties extends Component {
  state = {
    checked: "for-sale",
    searchType: "for-sale",
  }
  submit(ev) {
    debugger;
    ev.preventDefault();
    this.props.history.push(`/properties/search?type=${this.state.searchType}&q=${encodeURIComponent(this.state.q || '')}`);
    this.setState({ checked: null });
  }
  toggle(ev) {
    console.log("Toggle");
    this.setState(
      {
        checked: ev.target.id,
        searchType: ev.target.value
      },
      () => console.log(this.state)
    );
  }
  handleInputChange(ev) {
    let val = ev.target.value;
    this.setState({ q: val });
  }
  render() {
    let checkedID = (this.state || {}).checked || "for-sale";
    return (
      <main className="properties">
        <Banner
          imageURL={"/images/properties/banner.jpg"}
          height={770}
          overlay={{ alpha: 0.3 }}
          className="properties__banner"
        >
          <form method="post" onSubmit={ev => this.submit(ev)}>
            <ToggleGroup
              elementsData={toggleGroupData}
              checkedID={checkedID}
              onChange={(ev) => this.toggle(ev)}
            />
            <div className="properties__search">
              <input
                type="text"
                className="properties__search__input"
                name="q"
                placeholder="Country, Region, City, Postal Code or Property ID"
                onChange={ev => this.handleInputChange(ev)}
              />
              <button type="submit" className="properties__search__button btn">
                <i className="icon icon-search" />
                Search
              </button>
            </div>
          </form>
        </Banner>
      </main>
    );
  }
}
