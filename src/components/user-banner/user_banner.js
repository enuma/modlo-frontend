import React from 'react';

const UserBanner = ({
  avatar,
  name,
  role,
  properties,
  bookings
}) => {
  return(
    <section className= "user-banner">
      <div className = "container">
        <div className = "user-info">
          <div className = "user-avatar">
            <img src= { avatar } alt="Avatar"/>
          </div>
          <div className = "user-details">
            <h1>{ name }</h1>
            <small>{ role }</small>
            <button> Log out </button>
          </div>
        </div>
        <div className= "user-belongings">
          <div>
            <p>My Properties</p>
            <p className= "user-belongings__value">{ properties }</p>
          </div>
          <div>
            <p>My Bookings</p>
            <p className= "user-belongings__value">{ bookings }</p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default UserBanner;