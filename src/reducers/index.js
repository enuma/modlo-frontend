import countriesReducer, { countriesFlagsReducer } from "./countries";
import propertiesReducer, {
  propertiesFlagsReducer,
  MyPropertiesReducer,
  MyPropertiesFlagsReducer
} from "./properties";
import yachtsReducer, { yachtsFlagsReducer } from "./yachts";
import userReducer, { userFlagsReducer } from './user';
import { combineReducers } from "redux";
import { MyProperties } from "../components";

export const initialState = {
  countries: {},
  properties: {},
  yachts: {},
  countries_flags: {},
  properties_flags: {},
  yachts_flags: {},
  my_properties: [],
  my_properties_flags: {},
  user: {},
  user_flags: {},
};

export default combineReducers({
  countries: countriesReducer,
  properties: propertiesReducer,
  yachts: yachtsReducer,
  countries_flags: countriesFlagsReducer,
  properties_flags: propertiesFlagsReducer,
  yachts_flags: yachtsFlagsReducer,
  my_properties: MyPropertiesReducer,
  my_properties_flags: MyPropertiesFlagsReducer,
  user: userReducer,
  user_flags: userFlagsReducer,
});
