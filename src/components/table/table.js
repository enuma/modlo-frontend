import React from "react";
import { Link } from "react-router-dom";

function getTableData(data, heads, props) {
  if (data.length === 0) {
    return (
      <tr>
        <td className={"align-center"} colSpan={heads.length}>
          Currently, no data found.
        </td>
      </tr>
    );
  }
  const result = data.map((row, index) => {
    return (
      <tr key={index}>
        <td key={index}>{index + 1}</td>
        {heads.map((head, index) => {
          head = head.toLowerCase().replace(/\s+/g, "-");
          if (!typeof row[head] === "string") row[head] = row[head].toString();
          if(head === 'image') return <td key={index}><img src={row[head]}/></td>;
          return <td key={index}>{row[head]}</td>;
        })}
        {props.actions.show && (
          <td>
            <Link to={`/properties/${row.name}`}>Show</Link>
          </td>
        )}
      </tr>
    );
  });
  return result;
}

const getHeadData = heads => {
  const result = (
    <tr>
      {heads.map((head, index) => {
        return <th key={index}>{head}</th>;
      })}
    </tr>
  );
  return result;
};

const Table = props => {
  const { heads, data } = props;
  return (
    <div className="page-widget">
      <table className="table">
        <thead>{getHeadData(heads)}</thead>
        <tbody>{getTableData(data, heads, props)}</tbody>
      </table>
    </div>
  );
};

export default Table;
