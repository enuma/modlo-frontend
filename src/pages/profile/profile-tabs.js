import React, { Component } from 'react';
import { NavLink, Switch, Route, Router } from "react-router-dom";
import { AccountTabInformation, MyBookings, MyProperties, AccountTabEdit, AccountTabPassword } from '../../components';

class ProfileTabs extends Component {

  checkAccountActive( match, location) {
    if(match || location.pathname === '/profile/edit-account' || location.pathname === '/profile/change-password') {
      return true
    } 
    return false;
  }
  render() {
    const {user} = this.props;
    return <section className="profile-tabs-wrapper">
        <div className="profile-tabs container">
          <ul className="profile-tabs-nav">
            <li>
              <NavLink to={'/profile'} exact isActive={this.checkAccountActive}>
                Account
              </NavLink>
            </li>
            <li>
              <NavLink exact to={'/profile/bookings'}>
                My Bookings
              </NavLink>
            </li>
            <li>
              <NavLink exact to={'/profile/properties'}>
                My Properties
              </NavLink>
            </li>
          </ul>
          <div className="profile-tabs-content">
            <Switch>
              <Route render={props => <AccountTabInformation {...props} user={user} />} exact path="/profile" />
              <Route render={props => <MyBookings  {...props} user={user}/>} exact path="/profile/bookings" />
              <Route render={props => <MyProperties {...props} user={user} />} exact path="/profile/properties" />
              <Route render={props => <AccountTabEdit {...props} user={user} />} exact path="/profile/edit-account" />
              <Route render={props => <AccountTabPassword {...props} user={user} />} exact path="/profile/change-password" />
            </Switch>
          </div>
        </div>
      </section>;
  };
};

export default ProfileTabs;