import {APIInstance} from '../utils/api';

export const FETCH_COUNTRIES = {
  ROOT: 'fetch_countries',
  PENDING: 'fetch_countries_PENDING',
  FULFILLED: 'fetch_countries_FULFILLED',
  REJECTED: 'fetch_countries_REJECTED',
}

export function fetchCountries(contract_type, query) {
  return {
    type: FETCH_COUNTRIES.ROOT,
    payload: APIInstance.getCountries()
  }
}
