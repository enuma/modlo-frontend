import React from "react";
import { Field } from "formik";

const FieldWrapper = ({
  label,
  type,
  name,
  id,
  component,
  placeholder,
  onChange,
  onBlur,
  touched,
  errors
}) => {
  const _touched = touched[name];
  const _error = errors[name];
  return (
    <div className="form__input-group">
      {label && <label for={id}>{label}</label>}
      <Field
        className="form__input"
        type={type}
        component={component}
        name={name}
        id={id}
        placeholder={placeholder}
        onChange={onChange}
        onBlur={onBlur}
      />
      {_touched && _error && <p className="error">{_error}</p>}
    </div>
  );
};

export default FieldWrapper;
