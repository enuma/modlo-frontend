import _axios, { AxiosInstance } from 'axios';
// import { URLSearchParams } from 'url';
const BASE_URL = `https://modlo-back-end.herokuapp.com`;
const axiosInstance = _axios.create({
  baseURL: BASE_URL,
  method: 'get',
  headers: {
    'content-type': 'application/json'
  }
});

function constructQuery(qs) {
  let result = new URLSearchParams();
  Object.keys(qs).reduce((params: URLSearchParams, key) => {
    params.append(key, qs[key]);
    return params;
  }, result);
  return result;
}
// @flow

export class API {
  constructor(axiosInstance: AxiosInstance) {
    this.axios = axiosInstance;
  }

  updateTokens(headers) {
    const neededHeaders = {
      'access-token': headers['access-token'],
      client: headers['client'],
      expiry: headers['expiry'],
      'token-type': headers['token-type'],
      uid: headers['uid']
    };
    this.axios.defaults.headers.common = neededHeaders;
    localStorage.setItem('authHeaders', JSON.stringify(neededHeaders));
  }

  __search(property_kind, contract_type, query) {
    return this.axios.get(
      '/properties',
      constructQuery({
        property_kind,
        contract_type,
        query
      })
    );
  }

  __getPropertyObj(propertyID) {
    return this.axios.get(`/properties/${propertyID}`);
  }

  searchProperties(contract_type, query) {
    return this.__search('property', contract_type, query);
  }

  searchYachts(contract_type, query) {
    return this.__search('yacht', contract_type, query);
  }

  getProperty(propertyID) {
    return this.__getPropertyObj(propertyID);
  }

  getYacht(propertyID) {
    return this.__getPropertyObj(propertyID);
  }

  getCountries() {
    return this.axios.get('/countries');
  }

  login(email, password) {
    return this.axios.post('/auth/sign_in', { email, password }).then(res => {
      this.updateTokens(res.headers);
      localStorage.setItem('authUser', JSON.stringify(res.data));
      return res;
    });
  }

  logout() {
    return this.axios.delete('/auth/sign_out');
  }

  register(values) {
    return this.axios.post('/auth', values);
  }

  getMyProperties() {
    // To be changes when we get endpoint
    return this.__search('property', '', '');
  }
}

export const APIInstance = new API(axiosInstance);
