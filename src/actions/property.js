import {APIInstance} from '../utils/api';

export const FETCH_PROPERTIES = {
  ROOT: 'fetch_properties',
  PENDING: 'fetch_properties_PENDING',
  FULFILLED: 'fetch_properties_FULFILLED',
  REJECTED: 'fetch_properties_REJECTED',
}

export function fetchProperties(contract_type, query) {
  return {
    type: FETCH_PROPERTIES.ROOT,
    payload: APIInstance.searchProperties(contract_type, query),
    meta: {
      contract_type,
      query
    }
  }
}

export const FETCH_PROPERTY = {
  ROOT: 'fetch_property',
  PENDING: 'fetch_property_PENDING',
  FULFILLED: 'fetch_property_FULFILLED',
  REJECTED: 'fetch_property_REJECTED',
}

export function fetchProperty(propertyID) {
  return {
    type: FETCH_PROPERTY.ROOT,
    payload: APIInstance.getProperty(propertyID),
    meta: {
      propertyID
    }
  }
}

export const FETCH_MY_PROPERTIES = {
  ROOT: 'fetch_my_properties',
  PENDING: 'fetch_my_properties_PENDING',
  FULFILLED: 'fetch_my_properties_FULFILLED',
  REJECTED: 'fetch_my_properties_REJECTED',
}

export function fetchMyProperties() {
  return {
    type: FETCH_MY_PROPERTIES.ROOT,
    payload: APIInstance.searchProperties('', ''),
  }
}