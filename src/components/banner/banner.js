import React from "react";
import classNames from "classnames";

export default ({ className, imageURL, height, children, overlay }) => {
  overlay = overlay && { r: 0, g: 0, b: 0, alpha: 0.7, ...overlay };
  return (
    <div
      className={classNames("banner", className)}
      style={{
        height: height,
        backgroundImage: `url('${imageURL}')`,
        backgroundSize: "cover",
        backgroundPosition: "center"
      }}
    >
      {overlay && (
        <div
          className="banner__overlay"
          style={{
            backgroundColor: `rgba(${overlay.r}, ${overlay.g}, ${overlay.b}, ${
              overlay.alpha
            })`
          }}
        />
      )}
      {children && <div className="banner__content">{children}</div>}
    </div>
  );
};
